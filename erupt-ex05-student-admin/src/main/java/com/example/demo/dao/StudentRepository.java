package com.example.demo.dao;

import com.example.demo.model.Student;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * CREATE TABLE `t_student` (
 * 	`id` BIGINT(19,0) NOT NULL AUTO_INCREMENT,
 * 	`age` INT(10,0) NULL DEFAULT NULL,
 * 	`birthday` DATETIME NULL DEFAULT NULL,
 * 	`gender` BIT(1) NULL DEFAULT NULL,
 * 	`map` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_0900_ai_ci',
 * 	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_0900_ai_ci',
 * 	PRIMARY KEY (`id`) USING BTREE
 * )
 * COLLATE='utf8mb4_0900_ai_ci'
 * ENGINE=InnoDB
 * ;
 * @author fanhang
 */
@Repository
public interface StudentRepository extends CrudRepository<Student, Long> {
}