package com.example.demo.controller;

import com.example.demo.dao.StudentRepository;
import com.example.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author fanhang
 */
@RestController
public class StudentController {
    @Autowired
    private StudentRepository studentRepository;

    @RequestMapping("/list")
    public Iterable<Student> list() {
        return studentRepository.findAll();
    }

    @RequestMapping("/delete")
    public void delete(@RequestParam("id") long id) {
        studentRepository.deleteById(id);
    }

}