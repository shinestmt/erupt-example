package com.example.demo;

import org.springframework.context.annotation.Configuration;
import xyz.erupt.web.EruptSimpleAuthConfigurer;
import xyz.erupt.web.vo.EruptMenuVo;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.List;

/**
 * @author fanhang
 */
@Configuration
public class MyWebConfigurer extends EruptSimpleAuthConfigurer {
    private final List<EruptMenuVo> menuList = new ArrayList<>();

    public MyWebConfigurer() {
        // 简单登录, 指定用户名和密码
        super("admin", "123456");
        // 使用静态固定菜单
        menuList.add(new EruptMenuVo(1L, "stu-manage", "学生管理", "table", "Student", "", null));
    }

    @Override
    public List<EruptMenuVo> getMenus(HttpServletRequest request) {
        return this.menuList;
    }

}
