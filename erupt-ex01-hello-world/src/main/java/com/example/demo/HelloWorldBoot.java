package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import xyz.erupt.core.annotation.EruptScan;

/**
 * @author fanhang
 */
@SpringBootApplication
@EntityScan
@EruptScan("com.example")
@ComponentScan("com.example")
public class HelloWorldBoot {

    public static void main(String[] args) {
        SpringApplication.run(HelloWorldBoot.class, args);
    }

}
