package com.example.demo.model;

import lombok.Data;
import xyz.erupt.annotation.Erupt;
import xyz.erupt.annotation.EruptField;
import xyz.erupt.annotation.sub_field.Edit;
import xyz.erupt.annotation.sub_field.EditType;
import xyz.erupt.annotation.sub_field.View;
import xyz.erupt.annotation.sub_field.sub_edit.*;

@Data
@Erupt(name = "学生管理")
public class Student {
    @EruptField
    private Long id;

    @EruptField(
            views = @View(
                    title = "姓名", sortable = true
            ),
            edit = @Edit(
                    title = "姓名",
                    type = EditType.INPUT, search = @Search, notNull = true,
                    inputType = @InputType
            )
    )
    private String name;

    @EruptField(
            views = @View(
                    title = "性别", sortable = true
            ),
            edit = @Edit(
                    title = "性别",
                    type = EditType.BOOLEAN, search = @Search, notNull = true,
                    boolType = @BoolType
            )
    )
    private Boolean gender;

    @EruptField(
            views = @View(
                    title = "年龄", sortable = true
            ),
            edit = @Edit(
                    title = "年龄",
                    type = EditType.NUMBER, search = @Search, notNull = true,
                    numberType = @NumberType
            )
    )
    private Integer age;

}