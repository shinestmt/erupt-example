package com.example.demo.dao;

import com.example.demo.model.Student;

import java.util.*;

/**
 * 内存模拟数据库操作
 * @author fanhang
 */
public class StudentDao {
    private final Map<Long, Student> studentMap = new HashMap<>();

    public StudentDao() {
        Random random = new Random();
        // 生成演示数据
        for (long i = 0; i < 10; i++) {
            Student stu = new Student();
            stu.setId(i);
            stu.setName("name-" + i);
            stu.setGender(random.nextBoolean());
            stu.setAge(18 + random.nextInt(4));
            studentMap.put(i, stu);
        }
    }

    public List<Student> getAll() {
        return new ArrayList<>(studentMap.values());
    }

    public int delete(Long id) {
        studentMap.remove(id);
        return 1;
    }
}