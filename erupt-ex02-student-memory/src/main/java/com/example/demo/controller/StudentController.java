package com.example.demo.controller;

import com.example.demo.dao.StudentDao;
import com.example.demo.model.Student;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

/**
 * @author fanhang
 */
@RestController
public class StudentController {
    @Autowired
    private StudentDao dao;

    @RequestMapping("/list")
    public List<Student> list() {
        return dao.getAll();
    }

    @RequestMapping("/delete")
    public void delete(@RequestParam("id") long id) {
        dao.delete(id);
    }

}