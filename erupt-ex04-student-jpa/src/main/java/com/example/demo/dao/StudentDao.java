package com.example.demo.dao;

import com.example.demo.model.Student;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.util.List;

/**
 * CREATE TABLE `t_student` (
 * 	`id` BIGINT(19,0) NOT NULL AUTO_INCREMENT,
 * 	`age` INT(10,0) NULL DEFAULT NULL,
 * 	`birthday` DATETIME NULL DEFAULT NULL,
 * 	`gender` BIT(1) NULL DEFAULT NULL,
 * 	`map` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_0900_ai_ci',
 * 	`name` VARCHAR(255) NULL DEFAULT NULL COLLATE 'utf8mb4_0900_ai_ci',
 * 	PRIMARY KEY (`id`) USING BTREE
 * )
 * COLLATE='utf8mb4_0900_ai_ci'
 * ENGINE=InnoDB
 * ;
 * @author fanhang
 */
@Repository
public class StudentDao extends JdbcDaoSupport {

    public StudentDao(DataSource dataSource) {
        super.setDataSource(dataSource);
    }

    public List<Student> getAll() {
        String sql = "select id, age, birthday, gender, name from t_student";
        return getJdbcTemplate().queryForList(sql, Student.class);
    }

    public int delete(Long id) {
        String sql = "delete from t_student where id = ?";
        return getJdbcTemplate().update(sql, new Object[] {id});
    }
}