
### 模块说明：
  
| 工程                         | 描述                             |
|------------------------------|----------------------------------|
| erupt-ex01-hello-world       | 入门 Hello-World                 |
| erupt-ex02-student-memory    | 学生管理(内存数据)               |
| erupt-ex03-student-jdbc      | 学生管理(jdbc)                   |
| erupt-ex04-student-jpa       | 学生管理(jpa)                    |
| erupt-ex06-student-admin     | 学生管理(后台管理, UPMS登录认证) |


默认用户名密码： erupt / erupt

